import {AfterViewInit, Component, ElementRef, OnInit, ViewChild, Input} from '@angular/core';
import {BreakpointObserver, Breakpoints} from '@angular/cdk/layout';
import { IncomingDto } from 'src/app/models/IncomingDto';
import * as moment from 'moment';
import { DataSourceItem } from 'src/app/models/DataSourceItem';

@Component({
  selector: 'app-bar-chart',
  templateUrl: './bar-chart.component.html',
  styleUrls: ['./bar-chart.component.scss']
})
export class BarChartComponent implements OnInit {
  constructor(
    private breakpointObserver: BreakpointObserver
  ) {
    breakpointObserver.observe([
      Breakpoints.XSmall,
    ]).subscribe((result: any) => {
      if (result.matches) {
        this.layoutGap = '8px';
        this.containerHeight = '300px';
      }
    });

    breakpointObserver.observe([
      Breakpoints.Small,
    ]).subscribe((result: any) => {
      if (result.matches) {
        this.layoutGap = '24px';
        this.containerHeight = '400px';
      }
    });

    breakpointObserver.observe([
      Breakpoints.Medium,
    ]).subscribe((result: any) => {
      if (result.matches) {
        this.layoutGap = '24px';
        this.containerHeight = '500px';
      }
    });
  }

  @Input() consultants: IncomingDto[];

  @ViewChild('flexLayoutContainer', {static: false}) flexLayoutContainerElement: ElementRef;

  public elementStyle: object = {
    'height.px': 50
  };

  public containerStyle: object = {
    'width.px': 300
  };

  public containerHeight = '500px';
  public layoutGap = '24px';

  single: any[];
  multi: any[];

  view: any[] = [700, 400];

  // options
  showXAxis = true;
  trimXAxisTicks = false;
  showYAxis = true;
  gradient = false;
  showLegend = true;
  showXAxisLabel = false;
  xAxisLabel = 'Date';
  showYAxisLabel = false;
  yAxisLabel = 'Net Earnings';
  showGridLines = true;
  legendPosition = 'below';

  dataSource: DataSourceItem[] = [];
  consultantCountInDataSource = 0;

  ngOnInit() {
  }

  private setSize() {
    this.elementStyle['height.px'] = this.flexLayoutContainerElement.nativeElement.offsetHeight;
    this.containerStyle['width.px'] = this.flexLayoutContainerElement.nativeElement.clientWidth;
  }

  onSelect(event) {
    console.log(event);
  }
  resolveData() {
    if (this.consultants && this.consultants.length !== this.consultantCountInDataSource) {
      this.consultantCountInDataSource = this.consultants.length;
      this.dataSource = [];

      this.consultants.forEach(item => {
        if (item.incomingList) {
          item.incomingList.forEach(incoming => {
              const date = moment(incoming.referenceDate).format('MMM YYYY');
              const foundItem = this.dataSource.find(dataItem => dataItem.name === date);
              if (foundItem) {
                foundItem.series.push({
                  name: item.noUsuario,
                  value: incoming.netEarnings
                });
            } else {
                this.dataSource.push({
                  name: date,
                  series: [{
                    name: item.noUsuario,
                    value: incoming.netEarnings
                  }]
              });
            }});
        }
  });

      return this.dataSource;
  }

    return this.dataSource;
  }
}
