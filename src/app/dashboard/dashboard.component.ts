import { Component, OnInit } from '@angular/core';
import { ConsultantService } from '../consultant.service';
import { IncomingDto } from '../models/IncomingDto';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})

export class DashboardComponent implements OnInit {

  consultants: IncomingDto[];
  selectedConsultants: IncomingDto[] = [];
  loadingIncomings = false;
  loadingConsultants = false;
  consultantIncomings: IncomingDto[];
  dateFrom = new Date(2007, 0);
  dateTo = new Date(2007, 11);

  constructor(protected consultantService: ConsultantService) {
  }

  ngOnInit(): void {
    this.getConsultants();
  }

  getConsultants() {
    this.loadingConsultants = true;
    this.consultantService.getConsultants().pipe(
      finalize(() => {
        this.loadingConsultants = false;
      })
    ).subscribe(
      data => {
        // Success
        this.consultants = data['items'];
        console.log(data);
      },
      error => {
        console.error(error);
      }
    );
  }

  getConsultantIncoming() {
    this.loadingIncomings = true;

    this.consultantService.getConsultantIncoming(this.selectedConsultants, this.dateFrom, this.dateTo).pipe(
      finalize(() => {
        this.loadingIncomings = false;
      })
    )
      .subscribe(
        data => {
          // Success
          this.consultantIncomings = data['items'];
          console.log(data);
        },
        error => {
          console.error(error);
        }
      );
  }

  onSelectionChange(selectionControl) {
    this.selectedConsultants = [];
    selectionControl.selectedOptions.selected.forEach(element => {
      this.selectedConsultants.push(element.value);
    });

    this.getConsultantIncoming();
  }

  onDateFromChanged(event) {
    this.dateFrom = event.value;
    this.getConsultantIncoming();
  }

  onDateToChanged(event) {
    this.dateTo = event.value;
    this.getConsultantIncoming();
  }
}
